<?php

namespace Abc\Service;

use Twig_Environment;
use Twig_Loader_String;

class DocumentGeneratorTest extends \PHPUnit_Framework_TestCase
{
    /** @var string */
    private $basePath;
    /** @var PdfGenerator */
    private $subject;


    protected function setUp()
    {
        $this->basePath = dirname(__FILE__) . '/../../fixtures/';
        $this->subject  = new DocumentGenerator(dirname(__FILE__) . '/../../fixtures/cache');
    }


    public function testGenerateDocumentFromHtmlToFile()
    {

        $htmlFile    = dirname(__FILE__) . '/../../fixtures/print_header_footer.html';
        $outputFile  = dirname(__FILE__) . '/test.pdf';
        $htmlContent = file_get_contents($htmlFile);

        $output = $this->subject->generate($htmlContent, $this->basePath, $outputFile);

        $this->assertStringStartsWith('%PDF', $output, 'Error checking file type');
        $this->assertFileExists($outputFile, $outputFile . " was not generated.");
        unlink($outputFile);
    }


    public function testGenerateDocumentFromTemplate()
    {

        $htmlFile = dirname(__FILE__) . '/../../fixtures/templates/simpleTemplate.html.twig';
        $template = file_get_contents($htmlFile);

        $loader = new Twig_Loader_String();
        $twig   = new Twig_Environment($loader);

        $name        = 'Test Name';
        $htmlContent = $this->subject->renderTemplate(
            $twig,
            $template,
            array('name' => $name)
        );

        $this->assertRegExp('/' . $name . '/i', $htmlContent, 'Rendered string does not contain: ' . $name);
    }
}
 