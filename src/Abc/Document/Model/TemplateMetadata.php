<?php


namespace Abc\Document\Model;


class TemplateMetadata implements TemplateMetadataInterface
{
    
    /**
     * @var int
     *
     */
    protected $leftMargin;

    /**
     * @var int
     *
     */
    protected $rightMargin;

    /**
     * @var int
     *
     */
    protected $topMargin;

    /**
     * @var int
     *
     */
    protected $bottomMargin;

    /**
     * @var int
     *
     */
    protected $headerMargin;

    /**
     * @var int
     *
     */
    protected $footerMargin;

    /**
     * @var string
     *
     */
    protected $pageOrientation;


    /**
     * @return int
     */
    public function getLeftMargin()
    {
        return $this->leftMargin;
    }

    /**
     * @param int $leftMargin
     */
    public function setLeftMargin($leftMargin)
    {
        $this->leftMargin = $leftMargin;
    }

    /**
     * @return int
     */
    public function getRightMargin()
    {
        return $this->rightMargin;
    }

    /**
     * @param int $rightMargin
     */
    public function setRightMargin($rightMargin)
    {
        $this->rightMargin = $rightMargin;
    }

    /**
     * @return int
     */
    public function getTopMargin()
    {
        return $this->topMargin;
    }

    /**
     * @param int $topMargin
     */
    public function setTopMargin($topMargin)
    {
        $this->topMargin = $topMargin;
    }

    /**
     * @return int
     */
    public function getBottomMargin()
    {
        return $this->bottomMargin;
    }

    /**
     * @param int $bottomMargin
     */
    public function setBottomMargin($bottomMargin)
    {
        $this->bottomMargin = $bottomMargin;
    }

    /**
     * @return int
     */
    public function getHeaderMargin()
    {
        return $this->headerMargin;
    }

    /**
     * @param int $headerMargin
     */
    public function setHeaderMargin($headerMargin)
    {
        $this->headerMargin = $headerMargin;
    }

    /**
     * @return int
     */
    public function getFooterMargin()
    {
        return $this->footerMargin;
    }

    /**
     * @param int $footerMargin
     */
    public function setFooterMargin($footerMargin)
    {
        $this->footerMargin = $footerMargin;
    }

    /**
     * @return string
     */
    public function getPageOrientation()
    {
        return $this->pageOrientation;
    }

    /**
     * @param string $pageOrientation
     */
    public function setPageOrientation($pageOrientation)
    {
        $this->pageOrientation = $pageOrientation;
    }

}