<?php

namespace Abc\Document\Model;

interface TemplateMetadataInterface
{

    /**
     * @return int
     */
    public function getLeftMargin();

    /**
     * @param int $leftMargin
     */
    public function setLeftMargin($leftMargin);

    /**
     * @return int
     */
    public function getRightMargin();

    /**
     * @param int $rightMargin
     */
    public function setRightMargin($rightMargin);

    /**
     * @return int
     */
    public function getTopMargin();

    /**
     * @param int $topMargin
     */
    public function setTopMargin($topMargin);

    /**
     * @return int
     */
    public function getBottomMargin();

    /**
     * @param int $bottomMargin
     */
    public function setBottomMargin($bottomMargin);

    /**
     * @return int
     */
    public function getHeaderMargin();

    /**
     * @param int $headerMargin
     */
    public function setHeaderMargin($headerMargin);

    /**
     * @return int
     */
    public function getFooterMargin();

    /**
     * @param int $footerMargin
     */
    public function setFooterMargin($footerMargin);

    /**
     * @return string
     */
    public function getPageOrientation();

    /**
     * @param string $pageOrientation
     */
    public function setPageOrientation($pageOrientation);

}