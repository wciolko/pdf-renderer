<?php


namespace Abc\Service;

use Twig_Environment;

interface PdfGenerator
{
    /**
     * @param string      $html     HTML content to transform
     * @param string      $basePath Sets the base path, used for external stylesheets and images.
     * @param null|string $fileName Output file name
     * @return string PDF content
     */
    public function generate($html, $basePath, $fileName);



    /**
     * @param Twig_Environment $twig
     * @param string           $template   Template content
     * @param array            $parameters Template parameters
     * @return string Rendered template context
     */
    public function renderTemplate(Twig_Environment $twig, $template, $parameters);
}