<?php

namespace Abc\Service;

use Abc\Document\Model\TemplateMetadata;
use Abc\Document\Model\TemplateMetadataInterface;
use Twig_Environment;
use Mpdf\Mpdf;

class DocumentGenerator implements PdfGenerator
{

    /** @var Mpdf */
    protected $mpdf;

    public function __construct($cacheDir = '')
    {
        $tmpFolder = $cacheDir . '/tmp/';
        if (!is_dir($tmpFolder)) {
            mkdir($tmpFolder);
        }

        $fontDir = $cacheDir . '/ttfontdata/';
        if (!is_dir($fontDir)) {
            mkdir($fontDir);
        }

        if (!defined('_MPDF_TEMP_PATH')) {
            define("_MPDF_TEMP_PATH", $tmpFolder);
        }
        if (!defined('_MPDF_TTFONTDATAPATH')) {
            define("_MPDF_TTFONTDATAPATH", $fontDir);
        }

    }

    public function init(TemplateMetadataInterface $template, $mode = '', $format = 'A4', $defaultFontSize = 0, $defaultFont = '')
    {

        $this->mpdf = new Mpdf([
            'mode'              => $mode,
            'format'            => $format,
            'default_font_size' => $defaultFontSize,
            'default_font'      => $defaultFont,
            'margin_left'       => $template->getLeftMargin(),
            'margin_right'      => $template->getRightMargin(),
            'margin_top'        => $template->getTopMargin(),
            'margin_bottom'     => $template->getBottomMargin(),
            'margin_header'     => $template->getHeaderMargin(),
            'margin_footer'     => $template->getFooterMargin(),
            'orientation'       => $template->getPageOrientation()
        ]);
        $this->mpdf->SetTitle("Offer");
        $this->mpdf->SetAuthor("AboutCoders - Offer Generator");
        $this->mpdf->SetDisplayMode('fullpage');
    }

    /**
     * @param string      $html     HTML content to transform
     * @param string      $basePath Sets the base path, used for external stylesheets and images.
     * @param string|null $fileName Output file name
     * @return string PDF content
     */
    public function generate($html, $basePath, $fileName = null)
    {
        if (!$this->mpdf) {
            $metadata = new TemplateMetadata();
            $metadata->setLeftMargin(15);
            $metadata->setRightMargin(15);
            $metadata->setTopMargin(16);
            $metadata->setBottomMargin(16);
            $metadata->setHeaderMargin(9);
            $metadata->setFooterMargin(9);
            $metadata->setPageOrientation('P');
            $this->init($metadata);
        }

        $this->mpdf->basepath = $basePath;
        $this->mpdf->WriteHTML($html);

        if (!is_null($fileName)) {
            $this->mpdf->Output($fileName, 'F');
            return file_get_contents($fileName);
        } else {
            return $this->mpdf->Output('', 'S');
        }

    }


    /**
     * @param Twig_Environment $twig
     * @param string           $template   Template content
     * @param array            $parameters Template parameters
     * @return string Rendered template context
     */
    public function renderTemplate(Twig_Environment $twig, $template, $parameters)
    {
        return $twig->render($template, $parameters);
    }
}